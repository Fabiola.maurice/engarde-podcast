## This is the site for the **Guardian Project** podcast!

The site has been built using Jekyll. Individual posts for each episode power the RSS for Subscriptions via iTunes. Episodes can also be stream using the [Plyr](https://github.com/Selz/plyr) HTML/JS media player. 

The site design was created using [Jekyll Skeleton](https://github.com/timklapdor/jekyll-skeleton) by Tim Klapdor. 

The original project can be found here [https://github.com/timklapdor/link-rot](https://github.com/timklapdor/link-rot)
