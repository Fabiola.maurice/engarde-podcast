---
layout: post
title: "Clean Insights: Data, People and Dignity Roundtable with Data4Change, Tor Project, Simply Secure and Okthanks"
date: 2020-05-22 5:30:00 -500
file: https://guardianproject.info/podcast-audio/cise-podcast-datapeopledignity.mp3
summary: "Amazing designers talk about data, people and dignity!"
description: "A podcast roundtable that is part of the Clean Insights Symposium Extraordinaire - Amazing designers talk about data, people and dignity!"
duration: "46:34" 
length: "55986172"
explicit: "no" 
keywords: "clean insights, privacy preserving measurement, ethical analytics, open-source, design, dignity, data"
block: "no" 
voices: "Carrie Winfrey (Okthanks), Tiffany Robertson (Okthanks), Bronwen Robertson (Data4Change), Georgia Bullen (Simply Secure), Antonela Debiasi (Tor Project)"
---

### About this Episode

Amazing designers talk about data, people and dignity!

This podcast roundtable is part of the [Clean Insights Symposium Extraordinaire](https://cleaninsights.org/event)

#### Georgia Bullen, Executive Director, Simply Secure • @georgiamoon • she/her

Georgia has been an advocate in the internet health movement through her work and passion around issues such as net neutrality, security, privacy, and equitable access to technology. Her work has focused on the intersection of human-centered design, communities, physical space, and technology – e.g. how technology intersects with human rights — access to information and the right to communicate. She has a background in human-centered design, data visualization, urban planning and software development, and is an advocate for diversity in technology.

#### Bronwen Robertson, Data4Change, Director, co-founder

For nearly a decade Bronwen has supported civil society and human rights organisations around the world to use data-driven advocacy to amplify their stories through her work as Small Media’s director of research and global programmes. This work inspired her to identify pathways for civil society to use data for direct impact, and to co-found Data4Change. She is a published author and former UN Internet Freedom Fellow. 

#### Antonela Debiasi, Tor Project, pronoun.is/she IRC: antonela

Designer working with the UX team. Making Tor usable for everyone.

#### Tiffany Robertson, Community Ambassador, Okthanks

Tiffany is committed to justice. Working around the globe, she has accumulated diverse experiences that contribute to her effort to build awareness and support around social justice issues.

In Rwanda, Tiffany worked to empower women to build sustainable businesses with Keza, an ethical fashion venture. In London, she produced marketing profiles, improved member relations and helped coordinate the SOURCE Summit for the Ethical Forum Fashion. In Tennessee, she brightened days as a barista and social media marketing assistant with Sunnyside, a startup drive-thru coffee hut. In Ningbo, China, she taught English as a second language. In Alaska, Tiffany led visitors through the state’s terrain as a driver guide. Through each experience, Tiffany works with a purpose to understand how to develop better ways to communicate unfair issues to build support around social justice by companies and consumers.

#### Carrie Winfrey, Founder, Design lead, Okthanks, Guardian Project

An Interaction Designer by trade, Carrie works hand-in-hand with teams to craft clear, effective user experiences and brand messages. She has over 8 years of experience specializing in User Experience (UX) and User Interface (UI) design for mobile and web-based applications. She has worked with multiple startups to name their products and services, and to produce strong visual brand identities.

### Show Notes and Links
- [Okthanks](https://okthanks.com)
- [Simply Secure](https://simplysecure.org)
- [Data4Change](https://www.data4chan.ge/)
- [Music courtesy of Tim Kahn of Freesound](https://freesound.org/people/tim.kahn/sounds/28445/)


