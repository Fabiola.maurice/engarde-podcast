---
layout: post
title: "On Zoom, Contact Tracing and Walking Dogs in the Rain"
date: 2020-04-03 10:30:00 -500
file: https://guardianproject.info/podcast-audio/EnGardePodcast3-walk-the-dog.mp3
summary: "We're in a pandemic, but you still have to walk the dog, and privacy still matters... so let's talk about Zoom and Contact Tracing!"
description: "Learn how we've done our best over the years to support our users, and what we think the future of help desks softwares can and should be"
duration: "11:00" 
length: "4140317"
explicit: "no" 
keywords: "zoom, privacy, contact tracing, pandemic, security, free software, jitsi meet"
block: "no" 
voices: "Nathan Freitas (n8fr8)"
---

### Show Notes and Links

- [Citizen Lab report on Zoom](https://citizenlab.ca/2020/04/move-fast-roll-your-own-crypto-a-quick-look-at-the-confidentiality-of-zoom-meetings/)
- [The Intercept on Zoom's Encryption](https://theintercept.com/2020/04/03/zooms-encryption-is-not-suited-for-secrets-and-has-surprising-links-to-china-researchers-discover/)
- [Jitsi Meet](https://meet.jit.si/)
- [NextCloud Talk](https://nextcloud.com/talk/)
- [Riot.im / Matrix](https://about.riot.im/)
- [Trace Together](https://www.tracetogether.gov.sg/)
- [BlueTrace Manifesto](https://bluetrace.io/)
- [Safe Paths](https://covidsafepaths.org/)

### Transcript

(This is an automated transcript, please excuse any typos or hilarious mistakes)

It's a gray Friday here in New England where I'm recording this podcast. It's not the kind of weather. I want to see right now. I want to I'd rather be able to fling the windows open and have the house ful
l of fresh air and see the magnolia tree across the street blossoming. 

But the good news is my kids still took the dog for a walk. Whether be darned darned and I appreciate their heartiness in that and the hardiness of a lot of people around me in life right now who were weat
hering these challenging times? This is the third week or so of our lockdown or voluntary semi voluntary stay at home order in the area and homeschooling and we're learning a lot. 

I think everyone's learning a lot about technology that they need and rely on in their lives both personal and work. I know that zoom has been the in the news quite a bit. Recently due to its sudden surgeo
n popularity and use and the subsequent audits and investigations around just how secure and private it is. 

Soon hasn't had a great track record overall, but mostly was seen as bugs that needed to be fixed. They are close to us proprietary for-profit software as well. But the fact that it seems to just work with
 large groups and a variety of quality of connections has. Allowed zoom to really get great adoption quickly. 

I for one really like it because or liked it. I should say because there was no registration required to use it. A simple link got people into a meeting. And that means a lot for the kind of work I do and 
with online trainings and online education. I've used basically every video conference and remote, you know VoIP system for the last 20 years and zoom really offers something different and that maybe in te
rms of quality is only matched by FaceTime but Zoom works on Linux and the web and mobile and different than Android we're FaceTime doesn't. 

So yeah, so Zoom has gotten a lot of adoption. Now never did I think that Zoom off with the same level of privacy and security that something like signal offers or even you know wire or whatsapp. I knew th
at to achieve the sort of scaling of group communication that they have there is no way they were doing full kind of well implemented end encryption as again, we've come to expect. 

And perhaps. Apps. I failed in when I saw that they were labeling what they were doing and to end encryption and I didn't question it enough. While it's been murky what they're actually doing a new report 
today from citizen lab came out that really is excellent and digs into the details of. 

Their end-to-end encryption, you know, which they do seem to have. A kind of end-end encryption that relies on centralized key management and distribution. Such that you know, any sort of skilled attacker 
or legal order that could work from their infrastructure could intercept and compromise the end and encryption. This is also quite true because there is no way to verify keys or verify the security of a se
ssion. 

Now a lot of what I use zoom for are large kind of public meetings related to academic settings or trainings or things that really don't require the level of privacy and verified security of something like
 signal. But what I've seen is that the more I've used it for those cases or we've used it a guardian project or participated in communities using it the more it starts creeping into everything you're doin
g. 

We have maintained a certain number of our team meetings on using things like jitsi meet. You know a combination of IRC and matrix and jitsi as a way to communicate and in many cases are meetings are fairl
y open and and we are transparent in our work. And and that's worked well enough but at the same time zoom was being used by other parts of my community that you know started to rely on it for everything e
ven though we would say look if you if you need to do a one to one call with you security and privacy you should probably use signal or whatsapp if that's all that's possible. 

Or use encrypted voice messages through a matrix messenger, or you know, there's there's other than real-time calls. Anyhow, It's fairly disappointing what with the citizen lab report out today around the 
just the way zoom is set up there. Reliance on so much of infrastructure and teams in China brings concern and the poor. 

Choices they've continued to make you know, what before might have seen as bugs now seem like bad decision-making and lack of care. I don't care about the Facebook SDK so much bug before. Again, it's just 
lack it's it's a death by a thousand paper cuts of lack of care. 

So what to do? Well, we're recommitting ourselves to open communication solutions. We already use thing tools like matrix.org and riot and custom matrix clients that have voice messaging in them video mess
aging. We use jitsi meet and are going to be increasingly hosting our own but great communities, like, you know, may first and Caelix and others out there. 

There's many open public jitsi meet hosts that you can utilize. We also are a big fan of next cloud, which is a self hostable or. Fully open source. Cloud platform that has next cloud talk support built-in
 which can also work well. And we'll be looking into wire again as well for for group conversations wire is a open source competitor of sorts to both signal and whatsapp and slack and offers a lot of value
 through having. 

A great balance of features and not requiring a phone number. I think Zoom may still have its uses again for very large public conferences for doing press conferences for coordinating live streams where yo
u need multiple people to dial in and then do a live stream to YouTube or Facebook, like my church is doing for instance. 

It has its uses. But if you're you know, like citizen lab or the intercept is said ultimately if you're an activist human rights journalist and you're relying on on it for privacy that's probably not a goo
d solution. 

The other topic I want to touch on briefly and will come back to it more related to you know, the the kind of decisions we're making during this pandemic sort of response. You know zoom gained adoption bec
ause it was easy and it worked and people said, oh wow that was great. 

I need to stay connected. But as we evolve we can maybe make better decisions. Think at the same time, there's a lot of new technology both software and hardware being designed to support public health use
s of needs in in a epidemic in this pandemic. Related to tracking the location of people or tracking proximity of people near each other. 

So we've seen great efforts from the Singapore governments kind of open technology group to create trace together and now glue trace and open privacy preserving way to detect and log pro exhibit proximity 
to other devices other people and their phones through their phones. And that. Through that in case you tested positive you would be able to alert people that they should also isolate and quarantine themse
lves and get tested as soon as possible. 

We've seen MIT's safe paths project, which is using GPS location, but in an attempt to be a user privacy preserving methodology for storing and sharing that only as necessary. And now a joint kind of Europ
ean effort with similar approach and the use of kind of federations federated servers between countries and more integration into a public health workflow a government kind of data sharing workflow, which 
is also interesting. 

So these are all efforts. That we are excited to see. We are going to be keeping eye on related to the work we've done with privacy preserving measurement and analytics. We have some work we've been doing 
with activists and workers unions around this. And we should all be scrutinizing this heavily. 

You know, Google and Facebook and others are going to be increasingly using the location they have data they have for public health uses and governments have requested access to that data. And we need to b
alance right now between you know, the things we think are needed during a pandemic and what we will need to remain when we're out of this pandemic to maintain the privacy and dignity and and you know that
 that we deserve. 

So, In all cases whether it be zoom or a contact tracing solution, you know, keep in mind the balance between security and liberty and ask the right questions and and support researchers who are looking in
to this and let's not let people off the hook just because it works right now. 

And we'll be doing our best to guardian project to participate in those efforts to model best behavior and support our partners and organizations who need these kind of questions asked and helped deciding 
where to go. So get in touch we're thinking about all these things and I guess I'll see on Gypsy meet our talk or wire moving forward and signal please as always signal video is how I talk to my mother wor
ks great. 

All right, that's it for today's podcast. Thanks for tuning in and stay well stay soapy wash up where I'm asked. But also get some exercise in fresh air and walk the dog even if it's raining. Take care.

