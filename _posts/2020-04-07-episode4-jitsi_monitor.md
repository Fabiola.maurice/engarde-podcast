---
layout: post
title: "Moving Beyond the Confines of Zoom to a Marvelous Plentitude of Jitsi"
date: 2020-04-07 10:30:00 -500
file: https://guardianproject.info/podcast-audio/EnGardeHansApr7JitsiMonitor.mp3
summary: "Nathan talks with Hans-Christoph Steiner, longtime Guardian Project member, about the open-source voice and video conference tool Jitsi Meet, and his efforts to help monitor and hopefully improve the quality of those services"
description: "Nathan talks with Hans-Christoph Steiner, longtime Guardian Project member, about the open-source voice and video conference tool Jitsi Meet, and his efforts to help monitor and hopefully improve the quality of those services"
duration: "15:12" 
length: "18889027"
explicit: "no" 
keywords: "zoom, privacy, security, jitsi meet, open-source, federation"
block: "no" 
voices: "Nathan Freitas (n8fr8), Hans-Christoph Steiner (hansstatus)"
---

### About this Episode

Nathan talks with Hans-Christoph Steiner, longtime Guardian Project member, about the open-source voice and video conference tool Jitsi Meet, and his efforts to help monitor and hopefully improve the quality of those services.

Watch an excerpt from the conversation on the [Guardian Project YouTube Channel](https://www.youtube.com/watch?v=PM7q93cPe1E&feature=emb_title)

![hans and nathan](https://guardianproject.info/podcast-audio/hansepisode4.jpg)

["Chopin Etudes, Op. 25" recording courtesy of MusOpen](https://musopen.org/music/611-etudes-op-25/)


### Show Notes and Links

- [Hans](https://at.or.at/)
- [Jitsi Meet](https://meet.jit.si/)
- [Jitsi Monitor Page](https://guardianproject.gitlab.io/jitsi-monitor/)
- [Jitsi Monitor Open-Source Project](https://gitlab.com/guardianproject/jitsi-monitor)

### Transcript

(This is an automated transcript, please excuse any typos or hilarious mistakes)

Welcome to the end guard podcast a little. Hi I'm Nathan from guardian project welcome to the and guard podcast. I'm here with Hans Christoff. Steiner another long time basic co-founder of Guardian Project here and we're gonna we're on Jitsi today. 

I've invited him into my kids home gym or our workout space in quarantine, but he's now beaming and I feel like I'm right next to you. I can almost touch your face yeah, it's pretty exciting. But yeah the I've been playing with the idea of like co-presence and feeling more present with people, so I'm really it's not really working but I can see I'm looking right into your eyes, so I'm ready to listen. 

Be present but yeah, so we're on jitsi here and it's pretty great tell me about. Jitsi it's it's it's instead of zoom or hang out or Skype we're using a jitsi so what is jitsi all right, okay. I'm Jitsi is standard space free software that implements really nice simple video and audio conference rooms what I really love about it is that. 

Whoever there's no user accounts and whoever you just go in and name of your URL, whatever you want to name it whoever has that. URL can join the room. By just clicking on that URL cooking on that link but yet it's still has the ability to so you can jump into a room that you choose but then you can set a password if you need it to be access controlled. 

And to me I find that it. That kind of the old ways of that these kind of things are set up for about okay at the user account and password if they remember and the login and you have to maintain a buddy list and. That ends up being much more of a headache than you get in return yeah wherever these days everyone has a way to communicate outside of outside of the the thing that Jitsi despite the you're using to do the call, you know, you can you can send an email you can send a signal message you can send an exit PP message whatever you want message. 

Of course. 

So. You can just send someone the Lincoln they're in in the room if it and you're in a room that's what it feels like like a meeting in a real space for like, hey what room are we meeting in oh we're gonna meet in this room, all right? I'll see you there and and that works best and it's just a URL, which is the way the web works now another interesting thing is we're actually doing this on not on a jitsi, you know corp Jitsi dot org server we're on. 

May first which is cooperative we've long been a part of a member of for hosting and so, You said where we meeting and I said well we're in a room but we're on May 1st.org server and so what is how is it that jitsi has many cert like different servers versus you know, zoom or or skype or whatever just has one server. 

Um, so it's it's free software and they've also done a very good job of making it pretty easy to install and run your stuff and so it's the whole kind of collections it's very complicated software actually and a lot of pieces so WebRTC is a key piece of this this is one standard that SRTP is another standard and it's actually using XMPPs that's processing ancient messaging method to kind of sit. 

The whole calls but it's this huge complicated thing in a nice bundle free software. And to people I mean, there's hundreds of instances now and that and and and this is I mean, there's a lot of really key benefits of this provides that you can't get from the kind of big central service like a school or university or a church or a town can run their own it's a one key thing about any of these services regardless, whether it's free software or not is that there's not a metadata a happening in the call who's connecting to the room and at what time how long you know, who's chatting? 

I so if you can run your if you are more sensitive deprive the privacy issues of who's in your in your in your conferences and your meetings your organization could run its own instance and and know that who exactly has access to that editor, yeah. And. When you use a service that is, you know, one big system anything from zoom or Skype or Google Hangouts or whatever they get all the metadata regardless of what you do. 

Right. And so, I mean, we like to use, you know groups like Calix Institute or now may first people we know and trust who runs servers that have a track record of of defending digital rights. And so, that's an important aspect. And so you notice then that they were all of these jitsi meet servers, but that we didn't know very much about them. 

And you and so you decided to do something about that and created a new project or a script. Yeah, if it's most I wouldn't it's just the start of a project mostly just a proof of concept of the idea. So what? Yeah, there's tons people have started making lists just like wiki pages that are lists of public instances. 

And so you can say, oh, let's see are there any organizations that I know that are running one that I trust and I want to use for example my hacker space here in Vienna meta lab now has a new dizzy. Instance. So, that's what I would do. So the thing is that it's pretty hard to evaluate. 

The difference is between them. If especially if you don't really if you're not a technical person and I started looking at how disease configured and turns out a lot of the information is is kind of by default publicly available. So I write a lot of scrapers and I just I made a scraper that just takes the lists. 

I had a couple lists that I found that are people where people are updating the list of public instances. Just go through those URLs and looks for the public Jitsy configuration information and also the TLS settings so you can kind of evaluate whether they're doing a good job of setting up the the kind of standard security practice. 

And then just so in that process I discovered this a couple interesting things you can see there. One is Jitsi lets you as I kind of a let's you pre-configured analytics so you can see whether they've set up analytics on it. So, I mean usually means reporting data to an adolescence. 

Like Google Firebase or something like that. Another thing is that there's a one of the many moving pieces in it running to see as a thing called stun which helps clients find each other and and send traffic to each other by default. I think just to use the Google ones but a lot of people don't want to send data to Google so let's changeable that you can see. 

So, So that is a kind of a data point that people can say when they're evaluating or is just the right instance for me. Well it also then helps people so maybe the people who installed the, Digit the instance we're in aware of all these settings and these defaults so you can someone who's using it to say, hey did you know that you are Jitsi instances sending data to Google and so there's there's a kind of history of like compliance testers or analyzers for you know, website security or for your messaging server and so this is scanning that and then and then producing this report data, so if you go to the link, you showed us this huge sort of table and JSON data report and and you are asking you published it. 

The say here's what I've done now. I need some more help so how are you look what kind of help are you looking for right so yeah, my skills are it's scraping and gathering data and kind of the details and not so much in user interface design or implementation, certainly. 

So what why I have is a very rough table of the giant table of the data which is ugly but you can skin get it but it when there is also there is a JSON feed so if anyone is good at pretty standard web design should be able to just use that feed directly and and and and then show this information in a much more navigatable way there's also there's I mean just the possibilities of adding all sorts of things yeah, two others people find yeah, I think and I mean, I I think they're yeah they're some other it'll be interesting to see what the data you're collecting can reveal. 

I know there was also at the Berkman Klein Center one user was talking about her I think she's blind and her concerns with accessibility in software and that you know, the old system Berkman used Adobe Connect was horrible on accessibility, but zoom was actually quite good and I was happy to hear she was just able to use jitsi with 25 users to teach a class and she said it was great so there's I think interesting things around accessibility or around. 

Ping times and sort of speed and latency as well that might be important. Yeah yeah so are we one of the things that's cool about we're used to the internet thinking not, you know, not thinking about where the servers is located but it turns out when you're doing something live interactive with voice and video it actually it matters where it's located because it can make a very big difference in the sound quality in the call quality like how big of a delay there is between the time you speak in the person hears you so for example now with in quarantine times there's a lot of people in the same city who need to use video chat. 

So there's no reason for them to all go to a server that's in New York when they're in Vienna which is by chance, so it turns out there is you know, I I looked at one of these lists and probably didn't in fact, oh look there's a company which is a hosting company that's via based in Vienna and has servers in Vienna and if I use that one it it's noticeably yeah. 

I mean there isn't noticeably less gap between the cup between you know, when I talk and the next person hears it when it's interesting right now it because it's just two of us we actually, Have a direct. Your connection it said so that our video stream is not going through the server it's just between my home here and New England and your home and Austria and that's pretty amazing so that that that was easy enough and worked and these you know, these are the things the sort of improvements in user experience we need because right now I feel I mean, we should probably wrap this up because it's been going so well that I I would love to end this on a high note and just see you know, I think the software is is great a lot of it is just getting it deployed like you said on in logic. 

Ways having a lot of the things behind the scenes and software like skype or zoom that make it seem so good or the fact that they do these smart routing and they redirect you to a different server depending where you are and they have you know, millions of not billions of dollars to blow and venture capital funding to build capacity and they're also you know thinking about yeah that sort of you know what Skype used to call kind of super nodes where you would all route people around a certain geographic concentration, so I think your effort is great and that we can reveal some of these things but also, Allow users yeah to say oh yeah there is a server in my area I'll use that as opposed to just using the one called meet dot jit dot s i in which is going to get overloaded and you know, and and it's already starting it's already started so we need many of these to flourish just like there's many email servers many web servers out there in the world, it's very important, so thank you for your effort and initiative in this project yeah. 

I hope it's really deserves. I mean, I don't think it's a whole bunch it's just something I hope to plant the seeds of inspiration yeah. For people to say like, hey look here's all this data, let's let's make this useful let's help this drive, you know decentralizing the web and and letting people control the privacy that you know, the privacy concerns that they have and not you know, not be stuck using giant proprietary services because they're the only ones that that are actually that function right and we it's in our hands to support the jitsi team and and the company that owns them and in their efforts to be open source. 

And free software and allow modifications and improvements and you know, there's a lot to do so hopefully everyone can find a way to contribute right now to help help people stay connected, you know, I can I can't touch you I can touch your face here but it's a little high five a little high five ready put your hand up yeah. 

All right. I feel so close to you okay, all right well that's uh and guard for today, thank you Hans and check the monitor link below like and subscribe see you later. 

Oh no my phone storage. Well we'll see this is we got the audio at least. I got some of the video. Yeah, okay, oh yeah, I got a lot of video. I got about 10 minutes of the video, that's that's alright and I got all the audio, okay? I'm going to make.


