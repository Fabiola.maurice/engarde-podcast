---
layout: post
title: "Kaci of Okthanks on Design, People Being People and Moving On"
date: 2020-04-16 10:30:00 -500
file: https://guardianproject.info/podcast-audio/EnGardeKaciDesignApril162020.mp3
summary: "Nathan talks with Kaci B about her design work with Guardian Project over the last few years"
description: "Nathan talks with Kaci B about her design work with Guardian Project over the last few years"
duration: "52:44" 
length: "58843246"
explicit: "no" 
keywords: "design, human rights, internet freedom, yoga, humanity"
block: "no" 
voices: "Nathan Freitas (n8fr8), Kaci of Okthanks"
---

### About this Episode

Nathan talks with Kaci B of our amazing design partner [Okthanks](https://okthanks.com) on her work with us over the last few years. While we have always cared about the usability of our apps, we rarely had the ability to fund skilled professional designers to be part our team. This changed about six or seven years ago when we began to include meaningful funding and support for resources and time to work on usability and design in our development process. Through this we have been able to implement true co-design processes, participate in throughful workshop activities to gather feedback from our user communities, and create high quality websites and videos to market our work once it is done. These are what "regular people" expect in the world, even if your app is developed by communities they are a part of, and they think you are the greatest people in the world. People, especially those under durress, at risk or facing serious adversity, have little time or patience to use an app that doesn't make sense, isn't as "smooth", or is hard to explain. 

Working with great designers like Kaci has helped us along this journey, and we are sad to see her go, though happy for her future life!

You can watch this interview on the [Guardian Project YouTube Channel](https://www.youtube.com/watch?v=NxvAS9v-cIM)

![kaci and nathan](https://guardianproject.info/podcast-audio/EnGardeKaciDesignApril162020.jpg)

### Show Notes and Links

- [Okthanks](https://okthanks.com)
- [Onion Browser](https://onionbrowser.com)
- [Spotlight "WeClock" project](https://spotlightproject.gitlab.io/)
- [Circulo App](https://encirculo.org/)
- [SAVE by OpenArchive](https://open-archive.org/)
- Music: Tribute to Veena Raja Rao by Veena Kinhal is licensed under a Public Domain License. [Learn More](https://freemusicarchive.org/music/Veena_Kinhal/Tribute_to_Veena_Raja_Rao)



