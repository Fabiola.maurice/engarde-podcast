---
layout: post
title: "Clean Insights: Prof. James Mickens and Dr. Gina Helfrich on Ethics in Computer Science"
date: 2020-05-20 10:30:00 -500
file: https://guardianproject.info/podcast-audio/cise-ethics-in-compsci-may2020.mp3
summary: "Do Deep Fakes matter because they can fulfill childhood dreams of photorealistic Godzilla or an Avengers movie starring Gandi? Is tracking the heck out of your users fine, as long as it is you doing the tracking, or should ethical analytics mean more than self-hosting? How can we address the often myopic industry and academia to have fewer tech blind spots? We discuss these things and more!"
description: "Do Deep Fakes matter because they can fulfill childhood dreams of photorealistic Godzilla or an Avengers movie starring Gandi? Is tracking the heck out of your users fine, as long as it is you doing the tracking, or should ethical analytics mean more than self-hosting? How can we address the often myopic industry and academia to have fewer tech blind spots? We discuss these things and more!"
duration: "53:18" 
length: "74184704"
explicit: "no" 
keywords: "clean insights, privacy preserving measurement, ethical analytics, open-source"
block: "no" 
voices: "Nathan Freitas (n8fr8), Gina Helfrich (Internews), James Mickens (Harvard)"
---

### About this Episode

Nathan (n8fr8) of [Guardian Project](https://guardianproject.info) and Dr. Gina Helfrich of [Internews](https://globaltech.internews.org/our-resources/basics) welcome Professor James Mickens, of Harvard University's Computer Science department, to this third podcast panel of the [Clean Insights Symposium Extraordinaire](https://cleaninsights.org/event).

Do Deep Fakes matter because they can fulfill childhood dreams of photorealistic Godzilla or an Avengers movie starring Gandi? Is tracking the heck out of your users fine, as long as it is you doing the tracking, or should ethical analytics mean more than self-hosting? How can we address the often myopic industry and academia to have fewer tech blind spots? We discuss these things and more!

If you haven't had the pleasure of hearing Prof. Mickens give a talk, you have truly missed out on a unique voice of sobriety and hilarity in the field of Computer Science. In fact, you may just want to just pause the podcast now, and watch one his talks linked below. From the titles alone, you can tell you're in for something special.
- ["Why Do Keynote Speakers Keep Suggesting That Improving Security Is Possible?"](https://www.youtube.com/watch?v=ajGX7odA87k)
- ["My love letter to computer science is very short"](https://www.youtube.com/watch?v=GnBfGCUORK8)
- ["Blockchains Are a Bad Idea"](https://www.youtube.com/watch?v=15RTC22Z2xI)
- ["There are No Secrets"](https://www.youtube.com/watch?v=mDwUJa4_IJE)
- ["Life As A Developer: My Code Does Not Work Because I Am A Victim Of Complex Societal Factors..."](https://www.youtube.com/watch?v=7Nj9ZjwOdFQ)

#### James Mickens, Professor, Authority On All Things

Excellence. Quality. Science. These are just a few of the words that have been applied to the illustrious research career of James Mickens. In the span of a few years, James Mickens has made deep, fundamental, and amazing contributions to various areas of computer science and life. Widely acknowledged as one of the greatest scholars of his generation, James Mickens ran out of storage space for his awards in 1992, and he subsequently purchased a large cave to act as a warehouse/fortress from which he can defend himself during the inevitable robot war that was prophesied by the documentary movie “The Matrix.” In his spare time, James Mickens enjoys life, liberty, and the pursuit of happiness, often (but not always) in that order, and usually (almost always) while listening to Black Sabbath. 

#### Gina Helfrich, Internews
Dr. Gina Helfrich is Program Officer for Global Technology at Internews.
Previously, she served as Director of Communications and Culture at NumFOCUS, a non-profit that supports better science through open code. Gina is an accomplished and visionary leader with a track record of success across a variety of fields, including nonprofits, higher education, and business. She was co-founder of recruitHER, a women-owned recruiting & consulting firm committed to making the tech industry more inclusive and diverse. The brand strategy Gina developed and executed for recruitHER quickly earned national attention and a list of high-profile clients including Pandora, GitHub, Pinterest, and RunKeeper. She earned press features including stories in both Austin Monthly and Austin Woman magazines, a talk at SXSW, and an interview on the Stuff Mom Never Told You podcast.



### Show Notes and Links

- [James Mickens: Professor, Authority On All Things](https://mickens.seas.harvard.edu/)
- [Gina Helfrich: Communications, Diversity & Inclusion, Women in Tech & Business](http://ginahelfrich.com/)
- [Clean Insights Symposium Extraordinaire](https://cleaninsights.org/event)
- Music courtesy of Archive.org: [Here Comes The Circus](http://archive.org/details/HereComesTheCircus)

